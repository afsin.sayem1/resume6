import me from "./img/me.jpg";
import topLeft from "./img/top-left.png";
import selectors from "./js/selectors";

const TopBar = () => {
  let { qs_a, on, add, remove } = selectors;
  let dr_ = 0;

  const dark_ = () => {
    dr_ = !dr_;

    let obj1 = {
      "--bg": "#1c1c24",
      "--body-bg": "#1d1d1d",
      "--theme-bg": "#13131a",
      "--inboxSVG-bg-color": "#5f6641",
      "--inboxSVG-active-color": "var(--active-bg-color)",
      "--profile-menu-border": "#15f1e4",
      "--topBar-black-color": "#fff",
      "--subtitle-color": "#83838e",
      "--bottomTopBorder-color": "#fff",

      "--active-color": "#ffffff",
      "--active-bg-color": "#0162ff",

      "--placeholder-color": "#9b9ba5",
      "--toolHigglightBxBg": "#292932",
      "--inactive-color": "#292932",
      "--active-light-color": "#263d63",

      "--hard-hover": "#fff",
      "--light-hover": "#dedede",

      "--card-hover": "#2b2ecf",

      "--src-color": "#fff",

      "--tp-left-clr": "#fff",

      "--active-btn-bg": "#0162ff",
      "--active-btn-clr": "#fff",

      "--inactive1-btn-clr": "var(--subtitle-color)",
      "--inactive1-btn-bg": "#292932",

      "--inactive2-btn-bg": "var(--inactive1-btn-bg)",
      "--inactive2-btn-clr": "var(--subtitle-color)",

      "--svg-spcl": "#fff",
    };

    let obj2 = {
      "--bg": "#ffffff",
      "--body-bg": "#e5ecef",
      "--theme-bg": "#fafafb",

      "--moonSVG-color": "#94949f",
      "--inboxSVG-bg-color": "#ffffff",
      "--inboxSVG-active-color": "#0162ff",
      "--profile-menu-border": "#d6d6db",

      "--topBar-black-color": "#2f2f33",
      "--subtitle-color": "#83838e",

      "--bottomTopBorder-color": " #efefef",
      "--active-color": "#ffffff",
      "--active-bg-color": "#0162ff",
      "--placeholder-color": "#9b9ba5",

      "--toolHigglightBxBg": "#e8f2ff",

      "--inactive-color": "#f0f0f0",
      "--active-light-color": "#e1ebfb",

      "--hard-hover": "#f0f0f0",
      "--light-hover": "#a3a3b6",

      "--card-hover": "#2b2ecf",

      "--src-color": "#0162ff",
      "--tp-left-clr": "#000",

      "--active-btn-bg": "#0162ff",
      "--active-btn-clr": "#fff",

      "--inactive1-btn-clr": "#0162ff",
      "--inactive1-btn-bg": "#e1ebfb",

      "--inactive2-btn-bg": "#f0f0f0",
      "--inactive2-btn-clr": "var(--subtitle-color)",

      "--svg-spcl": "var(--active-bg-color)",
    };

    if (dr_) {
      const set_ = (elm, obj) => {
        for (const k in obj) {
          if (Object.hasOwnProperty.call(obj, k)) {
            const el = obj[k];

            elm.style.setProperty(k, el);
          }
        }
      };
      set_(qs_a(":root")[0], obj1);

      add(qs_a(".moonSVG")[0], "dark-light-svg");
    } else {
      const set_ = (elm, obj) => {
        for (const k in obj) {
          if (Object.hasOwnProperty.call(obj, k)) {
            const el = obj[k];

            elm.style.setProperty(k, el);
          }
        }
      };
      set_(qs_a(":root")[0], obj2);

      remove(qs_a(".moonSVG")[0], "dark-light-svg");
    }
  };

  return (
    <div className="topBarCls content-c">
      <div className="h-60px d-flx flx-x x-x-jc-sb x-y-c">
        <div className="content-c">
          <img src={topLeft} alt="milao" className="m-0-12px-0-0" />
          <span className="fs-18px fw-600 tpLg">Milao</span>
        </div>

        <div className="toggleNav d-flx flx-x x-y-c mobile_hide">
          <div className="crs-p active_">Experience</div>
          <div className="crs-p m-0-0-0-30px">Education</div>
          <div className="crs-p m-0-0-0-30px">Achievements</div>
        </div>

        <div className="d-flx flx-x x-y-c">
          <svg
            className="crs-p moonSVG m-0-8px-0-0"
            viewBox="0 0 24 24"
            onClick={() => {
              dark_();
            }}
          >
            <path d="M21 12.79A9 9 0 1111.21 3 7 7 0 0021 12.79z"></path>
          </svg>

          <div className="profile-menu d-flx flx-x x-y-c h-32px">
            <svg className="inboxSVG" viewBox="0 0 24 24">
              <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
            </svg>
          </div>

          <div className="content-c">
            <img src={me} alt="me" className="w-32px m-0-10px-0-0 b-rds-50%" />
            <span className="myName fw-500 small_mobile_hide">
              Anatoly Raklyar
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopBar;
