import jsonData from "./json/data.json";
import selectors_ from "./js/selectors";
import { useState } from "react";

//img
import img1 from "./img/topBg/1.jpg";
import img2 from "./img/topBg/2.jpg";
import img3 from "./img/topBg/3.jpg";
import img4 from "./img/topBg/4.jpg";
import img5 from "./img/topBg/5.jpg";
import img6 from "./img/topBg/6.jpg";
import img7 from "./img/topBg/7.jpg";
import img8 from "./img/topBg/8.jpg";

const BottomBigRight = () => {
  let { mk_arr, qs_a, css, add, remove } = selectors_;
  let [showDataOnRight, set_showDataOnRight] = useState({});
  let { work } = jsonData;
  let exparience_ = work.filter(() => true);
  let img_arr = [img1, img2, img3, img4, img5, img6, img7, img8];
  let logoCLRS = [
    { bg: "#2e2882", clr: "#feb0a5" },
    { bg: "#f76855", clr: "#052c49" },
    { bg: "#56acee", clr: "#ffffff" },
    { bg: "#1f2027", clr: "#e0e0e1" },
    { bg: "#ffe810", clr: "#010101" },
    { bg: "#fe5c60", clr: "#ffeaeb" },
    { bg: "#5d6cc0", clr: "#ffffff" },
    { bg: "#ea4d88", clr: "#ffffff" },
  ];
  //functions
  const obj_manupulate = (obj, e) => {
    set_showDataOnRight({
      img: img_arr[e],
      artificialImg: obj.company.slice(0, 2).toUpperCase(),
      topTitle: obj.company,

      position: obj.position,
      startDate: `${obj.start.month}/${obj.start.year}`,
      endDate: `${obj.end.month}/${obj.end.year}`,
      company:
        obj.company.length > 13 ? `${obj.company.slice(0, 13)}..` : obj.company,
      highlights: obj.highlights,
      summary: obj.summary,

      bg: logoCLRS[e].bg,
      clr: logoCLRS[e].clr,
    });
  };

  const handleSlideLeft = (e) => {
    let key_ = Number(e);

    mk_arr(qs_a(".ech_shw")).forEach((el) => {
      css(el, {
        display: "none",
      });
    });

    mk_arr(qs_a("[data-ech-plt-ex]")).forEach((el, i) => {
      if (i > 0) {
        css(el, {
          "margin-top": "20px",
        });
      }
    });

    mk_arr(qs_a("[data-ech-plt-ex]")).forEach((el) => {
      css(el, {
        display: "flex",
      });
    });

    mk_arr(qs_a(".dyn-content_")).forEach((el) => {
      css(el, {
        display: "flex",
      });
    });

    mk_arr(qs_a(".bck_icn")).forEach((el) => {
      css(el, {
        display: "inline-block",
      });
    });

    mk_arr(qs_a(".ech_plate_container")).forEach((el) => {
      css(el, {
        "grid-template-columns": "repeat(1, 1fr)",
      });
    });

    remove(qs_a(".absolute_right")[0], "scrollLFTAnim");
    setTimeout(() => {
      add(qs_a(".absolute_right")[0], "scrollLFTAnim");
    });

    obj_manupulate(exparience_[Number(key_)], key_);
  };

  const handleSlideLeft2 = (e) => {
    let key_ = Number(e);
    obj_manupulate(exparience_[Number(key_)], key_);
  };

  const goBack = () => {
    mk_arr(qs_a(".ech_shw")).forEach((el) => {
      css(el, {
        display: "block",
      });
    });

    mk_arr(qs_a("[data-ech-plt-ex]")).forEach((el) => {
      css(el, {
        display: "none",
      });
    });

    mk_arr(qs_a(".dyn-content_")).forEach((el) => {
      css(el, {
        display: "none",
      });
    });

    mk_arr(qs_a(".bck_icn")).forEach((el) => {
      css(el, {
        display: "none",
      });
    });

    mk_arr(qs_a(".ech_plate_container")).forEach((el) => {
      css(el, {
        "grid-template-columns":
          window.innerWidth > 710 ? "repeat(3, 1fr)" : "repeat(1, 1fr)",
      });
    });

    remove(qs_a(".bigRight")[0], "scrollUpAnim");
    setTimeout(() => {
      add(qs_a(".bigRight")[0], "scrollUpAnim");
    });
  };

  return (
    <div className="bigRight scrollUpAnim p-0-0-0-40px">
      <div className="d-flx flx-x x-y-c w-100%">
        <div className="fs-19px fw-600 content-c">
          <span className="crs-p m-0-10px-0-0 bck_icn" onClick={() => goBack()}>
            &#8592;
          </span>

          <span>
            Showing {work.length > 9 ? `${work.length}` : `0${work.length}`}{" "}
            Items
          </span>
        </div>

        <div className="m-0-0-0-auto fs-14px">
          Sort by: <span className="newPST">Newest Post</span>{" "}
          <span className="bigRTmenuIcon">▼</span>
        </div>
      </div>

      <div className="ech_plate_container">
        {exparience_.map(
          (
            { company, start, end, summary, highlights, position, startToEnd },
            i
          ) => (
            <div
              key={i}
              data-key={i}
              className="ech_plate crs-p ech_shw"
              onClick={() => {
                handleSlideLeft(i);
              }}
            >
              <div
                className="uperCs content-c"
                style={{
                  backgroundColor: logoCLRS[i].bg,
                  color: logoCLRS[i].clr,
                }}
              >
                {company.slice(0, 2).toUpperCase()}
              </div>
              <div className="title_clr m-16px-0-0-0">{company}</div>
              <div className="summaryClr m-14px-0-0-0">
                {summary.length > 120
                  ? `${summary.slice(0, 120)}..`
                  : `${summary}`}
              </div>

              <div>
                <button className="detailMiniBtn mini-btn-hover m-14px-0-0-0">
                  {position}
                </button>
                <button className="detailMiniBtn mini-btn-hover m-14px-0-0-8px">
                  {startToEnd}
                </button>
              </div>

              <div className="w-100% d-flx flx-wrp-n">
                <button className="w-100% eventMiniBtn eventActiveBtn crs-p m-14px-0-0-0 fs-12px">
                  Apply Now
                </button>
                <button className="w-100% eventMiniBtn crs-p m-14px-0-0-12px fs-12px">
                  Messages
                </button>
              </div>
            </div>
          )
        )}

        <div className="absolute_right d-flx flx-x w-100%">
          <div className="w-330px d-flx flx-y small_mobile_hide">
            {exparience_.map(
              (
                {
                  company,
                  start,
                  end,
                  summary,
                  highlights,
                  position,
                  startToEnd,
                },
                i
              ) => (
                <div
                  key={i}
                  data-key={i}
                  data-ech-plt-ex
                  className="ech_plate crs-p ech_dnt_shw flx-y w-330px b-rds-8px"
                  onClick={() => {
                    handleSlideLeft2(i);
                  }}
                >
                  <div className="d-flx flx-x">
                    <div
                      className="uperCs content-c"
                      style={{
                        backgroundColor: logoCLRS[i].bg,
                        color: logoCLRS[i].clr,
                      }}
                    >
                      {company.slice(0, 2).toUpperCase()}
                    </div>

                    <div className="m-0-0-0-auto">
                      <div className="title_clr m-4px-0-0-0 fs-14px hard-hover">
                        {company.length > 27
                          ? `${company.slice(0, 27)}..`
                          : `${company}`}
                      </div>

                      <div className="summaryClr m-4px-0-0-0 fs-12px light-hover">
                        {summary.length > 27
                          ? `${summary.slice(0, 27)}..`
                          : `${summary}`}
                      </div>
                    </div>

                    <div className="m-0-0-0-auto">
                      <svg className="heart" viewBox="0 0 24 24">
                        <path d="M20.8 4.6a5.5 5.5 0 00-7.7 0l-1.1 1-1-1a5.5 5.5 0 00-7.8 7.8l1 1 7.8 7.8 7.8-7.7 1-1.1a5.5 5.5 0 000-7.8z"></path>
                      </svg>
                    </div>
                  </div>

                  <div className="d-flx flx-x x-x-jc-sb x-y-c">
                    <div>
                      <button className="detailMiniBtn mini-btn-hover m-14px-0-0-0">
                        {position}
                      </button>
                      <button className="detailMiniBtn mini-btn-hover m-14px-0-0-8px">
                        {startToEnd}
                      </button>
                    </div>

                    <div className="m-14px-0-0-0 fs-12px">
                      <span className="new_spn hard-hover">New</span>
                      <span className="m-0-0-0-8px d4_spn light-hover">4d</span>
                    </div>
                  </div>
                </div>
              )
            )}
          </div>

          <div className="dyn-content_ m-0-0-0-40px">
            <div>
              <div>
                <div
                  className="topLrgImgCont"
                  style={{
                    backgroundImage: `url(${showDataOnRight.img})`,
                  }}
                ></div>

                <div>
                  <p
                    className="uperCs content-c"
                    style={{
                      transform: "translate(80%,-50%) scale(1.5)",
                      backgroundColor: showDataOnRight.bg,
                      color: showDataOnRight.clr,
                    }}
                  >
                    {showDataOnRight.artificialImg}
                  </p>
                </div>

                <div className="d-flx p-25px-25px-20px-25px">
                  <div>
                    <p className="topTtle fs-20px fw-600">
                      {showDataOnRight.topTitle}
                    </p>
                  </div>

                  <div className="d-flx">
                    <svg className="heart hrt_ext" viewBox="0 0 24 24">
                      <path d="M20.8 4.6a5.5 5.5 0 00-7.7 0l-1.1 1-1-1a5.5 5.5 0 00-7.8 7.8l1 1 7.8 7.8 7.8-7.7 1-1.1a5.5 5.5 0 000-7.8z"></path>
                    </svg>

                    <svg viewBox="0 0 24 24" className="shareSVG m-0-0-0-12px">
                      <circle cx="18" cy="5" r="3"></circle>
                      <circle cx="6" cy="12" r="3"></circle>
                      <circle cx="18" cy="19" r="3"></circle>
                      <path d="M8.6 13.5l6.8 4M15.4 6.5l-6.8 4"></path>
                    </svg>
                  </div>
                </div>
              </div>

              <div className="d-flx flx-y p-0-25px-30px-25px">
                <div className="d-flx flx-x">
                  <div>
                    <span className="pat fs-14px fw-600">Patreon</span>
                    <span className="pat_ fs-12px fw-500">
                      &nbsp; - Londontowne, MD
                    </span>
                  </div>

                  <div className="pat_ fs-12px d-flx flx-x x-x-e">
                    <span>Posted 8 days ago</span>
                    <span className="fw-500">&nbsp; - 98 Application</span>
                  </div>
                </div>

                <div className="table_bx p-0-16px b-rds-8px">
                  <div>
                    <p className="pat_ fs-12px line-h-40px">Position</p>
                    <p className="pat__ fw-500 fs-12px p-0-0-10px-0">
                      {showDataOnRight.position}
                    </p>
                  </div>

                  <div>
                    <p className="pat_ fs-12px line-h-40px">Start</p>
                    <p className="pat__ fw-500 fs-12px p-0-0-10px-0">
                      {showDataOnRight.startDate}
                    </p>
                  </div>

                  <div>
                    <p className="pat_ fs-12px line-h-40px">End</p>
                    <p className="pat__ fw-500 fs-12px p-0-0-10px-0">
                      {showDataOnRight.endDate}
                    </p>
                  </div>

                  <div>
                    <p className="pat_ fs-12px line-h-40px">company</p>
                    <p className="pat__ fw-500 fs-12px p-0-0-10px-0">
                      {showDataOnRight.company}
                    </p>
                  </div>
                </div>
              </div>

              <div className="d-flx flx-y p-0-25px-20px-25px">
                <div>
                  <p className="pat__1">Summary</p>

                  <p className="pat_ fs-13px">{showDataOnRight.summary}</p>
                </div>

                <div className="m-30px-0-0-0">
                  <p className="pat__1">Highlights</p>

                  <p className="pat_ fs-13px line-h-2em">
                    {showDataOnRight.highlights}
                  </p>
                </div>
              </div>
            </div>

            <div></div>

            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BottomBigRight;
