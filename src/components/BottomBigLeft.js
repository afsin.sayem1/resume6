//jsonData
import jsonData from "./json/data.json";

import { useEffect, useState } from "react";

//cssJs
import cssJs from "./js/css";

const BottomBigLeft = () => {
  let [tool_key, set_tool_key] = useState("");
  let { skills } = jsonData;
  let tecs_ = skills.filter(
    ({ name, keywords }) =>
      name === "Technologies" &&
      keywords.toLocaleLowerCase().includes(tool_key.toLocaleLowerCase())
  );
  let dt_sci = skills.filter(
    ({ name, keywords }) =>
      name === "Data Science" &&
      keywords.toLocaleLowerCase().includes(tool_key.toLocaleLowerCase())
  );
  //functions
  const handleKeyPress = (e) => {
    set_tool_key(e.target.value);
  };
  useEffect(() => {
    cssJs();
  }, [tool_key]);

  return (
    <div className="bigLeft middle_mobile_hide">
      <div className="toolHighlightBx d-flx flx-y y-y-s p-24px-18px b-rds-8px">
        <div className="tl_1 fs-14px fw-500 m-0-0-8px-0">Searching Tool</div>
        <div className="tl_2 fs-13px m-0-0-20px-0">
          Search a tool here now and never miss a tool
        </div>

        <input
          className="tl_3 p-10px b-rds-6px"
          type="text"
          placeholder="Enter tool keyword"
          onChange={(e) => {
            handleKeyPress(e);
          }}
        />

        <button className="tl_4 m-14px-0-0-0 p-8px-10px fs-13px fw-600 b-rds-6px br-none">
          Search
        </button>
      </div>

      <div className="tl_h1 fs-14px fw-500 p-20px-0-0-0">Technologies</div>
      <div className="tl_ul1 fs-15px m-20px-0-0-0">
        {tecs_.map(({ keywords, value }, i) => (
          <div className="d-flx w-100%" key={i}>
            <div className="d-flx w-100%">
              <input
                className="toolstyle"
                type="checkbox"
                name="tecs"
                id={`tec_${i}`}
              />

              <label htmlFor={`tec_${i}`} className="fs-13px">
                {keywords}
              </label>
              <span className="tl_spn m-0-0-0-auto">{value}</span>
            </div>
          </div>
        ))}
      </div>

      <div className="tl_h2 fs-14px fw-500 p-20px-0-0-0">Data Science</div>
      <div className="tl_ul2 fs-15px m-20px-0-0-0">
        {dt_sci.map(({ keywords, value }, i) => (
          <div className="d-flx w-100%" key={i}>
            <div className="d-flx w-100%">
              <input
                className="toolstyle"
                type="checkbox"
                name="tecs"
                id={`dt_sci${i}`}
              />

              <label htmlFor={`dt_sci${i}`} className="fs-13px">
                {keywords}
              </label>
              <span className="tl_spn m-0-0-0-auto">{value}</span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BottomBigLeft;
