import BottomBigLeft from "./BottomBigLeft";
import BottomBigRight from "./BottomBigRight";
import selectors from "./js/selectors";

const BottomBar = () => {
  let { qs_a, css } = selectors;

  const handleFcs = () => {
    css(qs_a(".srcIconSVG")[0], {
      display: "none",
    });
  };

  const handleBlr = () => {
    css(qs_a(".srcIconSVG")[0], {
      display: "block",
    });
  };

  return (
    <div className="bottomBarCls">
      <div className="w-100% d-flx p-0-0-0-20px">
        <div className="srcLst ps-r h-56px">
          <div className="src-itm src-itm-1 content-c small_mobile_hide">
            <span>Technologies</span>
            <span className="fs-10px fw-600 m-0-0-0-5px">&#x2715;</span>
          </div>

          <div className="src-itm src-itm-2 content-c small_mobile_hide">
            <span>Data Science</span>
            <span className="fs-10px fw-600 m-0-0-0-5px">&#x2715;</span>
          </div>

          <input
            type="text"
            className="btmTopFstInput h-100%"
            onFocus={() => handleFcs()}
            onBlur={() => {
              handleBlr();
            }}
          />
        </div>

        <div className="srcInfo srcInfo-1 mobile_hide">
          <svg viewBox="0 0 24 24" className="locationSVG">
            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0118 0z"></path>
            <circle cx="12" cy="10" r="3"></circle>
          </svg>

          <p className="fs-14px">Londontowne, MD</p>
        </div>

        <div className="srcInfo srcInfo-2 mobile_hide">
          <svg className="jobSVG" viewBox="0 0 24 24">
            <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
            <path d="M16 21V5a2 2 0 00-2-2h-4a2 2 0 00-2 2v16"></path>
          </svg>

          <input type="text" placeholder="Job Type" />
        </div>

        <div className="srcInfo srcInfo-3 mobile_hide">
          <svg className="salarySVG" viewBox="0 0 24 24">
            <path d="M12.6 18H9.8a.8.8 0 010-1.5h2.8a.9.9 0 000-1.8h-1.2a2.4 2.4 0 010-4.7h2.8a.8.8 0 010 1.5h-2.8a.9.9 0 000 1.8h1.2a2.4 2.4 0 010 4.7z"></path>
            <path d="M12 20a.8.8 0 01-.8-.8v-2a.8.8 0 011.6 0v2c0 .5-.4.8-.8.8zM12 11.5a.8.8 0 01-.8-.8v-2a.8.8 0 011.6 0v2c0 .5-.4.8-.8.8z"></path>
            <path d="M21.3 23H2.6A2.8 2.8 0 010 20.2V3.9C0 2.1 1.2 1 2.8 1h18.4C22.9 1 24 2.2 24 3.8v16.4c0 1.6-1.2 2.8-2.8 2.8zM2.6 2.5c-.6 0-1.2.6-1.2 1.3v16.4c0 .7.6 1.3 1.3 1.3h18.4c.7 0 1.3-.6 1.3-1.3V3.9c0-.7-.6-1.3-1.3-1.3z"></path>
            <path d="M23.3 6H.6a.8.8 0 010-1.5h22.6a.8.8 0 010 1.5z"></path>
          </svg>

          <input type="text" placeholder="Salary Range" />
        </div>

        <button className="fndJB crs-p">Find Job</button>
      </div>

      <div className="bigsContainer p-30px-0-0-0">
        <BottomBigLeft />
        <BottomBigRight />
      </div>
    </div>
  );
};

export default BottomBar;
